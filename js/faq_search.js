
$(document).ready(function(){
  
  $("#edit-keyword").keyup(function(e){ 
    e.preventDefault();
    faq_search_ajax();
  });
	
  function faq_search_ajax(){ 
    $("#faq-results").slideDown(); 
    var search_val=$("#edit-keyword").val();
    if (search_val.length != "") {
      $.post("/faq_find", {keyword : search_val}, function(data){
        if (data.length > 0){ 
          $("#faq-results").html(data); 
        }
      })
    } else {
      $("#faq-results").html("");
      $("#faq-results").css("display", "none");
    }
  }

});