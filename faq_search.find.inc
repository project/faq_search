<?php
/**
  * @file
  * Performs the database query based on the supplied keywords.
  */

function faq_search_find() {

  $term = strip_tags(drupal_substr($_POST['keyword'], 0, 100));
  $query = "SELECT DISTINCT fq.question, fq.nid
  FROM {faq_questions} AS fq,
  {node_revisions} AS f
  WHERE fq.question LIKE '%%%s%%'
  OR fq.detailed_question LIKE '%%%s%%'
  OR f.body LIKE '%%%s%%'
  AND f.nid = fq.nid
  AND f.vid = fq.vid
  ORDER BY fq.question asc";
  $result = db_query($query, $term, $term, $term);
  $string = "";
  
  while ($row = db_fetch_object($result)) {
    $string .= "<a href='/". drupal_get_path_alias('node/'. $row->nid)."'>". $row->question ."</a>";
  }

  if ( empty($string) ) {
    $string = t("<p class='message'>No matches!</p>");
  }

  echo $string;
  exit;
}